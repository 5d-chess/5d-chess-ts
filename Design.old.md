# Design

## Objectives

Main high level objectives (in order of priority) of this library design

1) Stable and reliable function
   - Accurate operation give a better foundation for projects using this library
   - Pure function internals ensures consistency
2) Simple design for easier development
   - Makes maintenance and bug hunting faster and easier
3) Reasonable performance
   - Use hypercuboid checkmate detection
   - Cache results where possible (memoization)
   - Object -> Array interface if needed
4) Backwards compatible with 5d-chess-js
   - Minimizes disruptions during migration

## Scope

Beyond the bare basics game handling, 5D Chess TS is designed to fulfill three use cases:
1) Serializable state for server friendly applications
2) Illegal game functions for engines and analysis
3) Add additional GUI friendly data  

This library will need to:
 - Handle basic game logic
   - Allow passing and reverting actions
 - Full support of 5DPGN (with extensions) with comments
 - Full support of 5DFEN import and export
 - Calculate and return information relevant for GUI display
   - Real end information for moves
   - Deconstructed PGN
   - Display friendly version of available variants

## Architecture Diagram

WIP

```mermaid
classDiagram
  class Chess {
    _Session session

    createSession(variant? : VariantFullString | VariantString | VariantShortString) void
    createSessionFromPGN(pgn : string) void

    addMetadata(name : string, value : string) : void
    addComment(comment : string, actionHistoryIndex? : number, moveBufferIndex? : number) void

    canMove() boolean
    move(move : MoveInput) void
    moveWithPGN(pgn : string) void

    canUndo() boolean
    undo() void

    canSubmit() boolean
    submit() void

    pass() void

    canRevert() boolean
    revert() void

    getSession() Session
    getSessionPGN(displayOptions? : PGNDisplayOptions) string
    getSessionPGNInfo() PGNToken[]

    getMoves() Move[]
    getMovesPGN(displayOptions? : PGNDisplayOptions) string
    getMovesPGNInfo() PGNToken[]

    getChecks() Move[]
    getChecksPGN(displayOptions? : PGNDisplayOptions) string
    getChecksPGNInfo() PGNToken[]

    getVariants()$ Variant[]

    isCheck() boolean
    isSoftmate() boolean
    isCheckmate() boolean
  }
```

```mermaid
classDiagram
  direction LR

  class Chess
  Chess --> createSession : Chess.createSession
  Chess --> createSessionFromPGN : Chess.createSessionFromPGN
  Chess --> addSessionMetadata : Chess.addMetadata
  Chess --> addSessionComment : Chess.addComment
  Chess --> canMove : Chess.canMove
  Chess --> move : Chess.move
  Chess --> moveWithPGN : Chess.moveWithPGN
  Chess --> canUndo : Chess.canUndo
  Chess --> undo : Chess.undo
  Chess --> canSubmit : Chess.canSubmit
  Chess --> submit : Chess.submit
  Chess --> pass : Chess.pass
  Chess --> canRevert : Chess.canRevert
  Chess --> revert : Chess.revert
  Chess --> getSession : Chess.getSession
  Chess --> getSessionPGN : Chess.getSessionPGN
  Chess --> getSessionPGNInfo : Chess.getSessionPGNInfo
  Chess --> getMoves : Chess.getMoves
  Chess --> getMovesPGN : Chess.getMovesPGN
  Chess --> getMovesPGNInfo : Chess.getMovesPGNInfo
  Chess --> getChecks : Chess.getChecks
  Chess --> getChecksPGN : Chess.getChecksPGN
  Chess --> getChecksPGNInfo : Chess.getChecksPGNInfo

  class createSession {
    createSession(variant? : VariantFullString | \n VariantString | \n VariantShortString) _Session
  }
  createSession --> createGame

  class createSessionFromPGN {
    createSessionFromPGN(pgn : string) _Session
  }
  createSessionFromPGN --> getMetadataFromPGN
  createSessionFromPGN --> addSessionMetadata

  class addSessionMetadata {
    addSessionMetadata(session : _Session, \n name : string, \n value : string) _Session
  }

  class addSessionComment {
    addSessionComment(session : _Session, \n comment : string, \n actionHistoryIndex? : number, \n moveBufferIndex? : number) _Session
  }

  class canMove {
    canMove(game : _Game) boolean
  }

  class move {
    move(game : _Game, \n move : MoveInput) _Game
  }
  move --> canMove

  class moveWithPGN {
    moveWithPGN(game : _Game, \n pgn : string) _Game
  }
  moveWithPGN --> move

  class canUndo {
    canUndo(game : _Game) boolean
  }

  class undo {
    undo(game : _Game) _Game
  }
  undo --> canUndo

  class canSubmit {
    canSubmit(game : _Game) boolean
  }

  class submit {
    submit(game : _Game) _Game
  }
  submit --> canSubmit

  class pass {
    pass(game : _Game) _Game
  }

  class canRevert {
    canRevert(game : _Game) boolean
  }

  class revert {
    revert(game : _Game) _Game
  }
  revert --> canRevert

  class getSession {
    getSession(session : _Session) Session
  }

  class getSessionPGN {
    getSessionPGN(session : _Session, \n displayOptions? : PGNDisplayOptions) string
  }
  getSessionPGN --> getSessionPGNInfo

  class getSessionPGNInfo {
    getSessionPGNInfo(session : _Session) PGNToken[]
  }

  class getMoves {
    getMoves(game : _Game) Move[]
  }

  class getMovesPGN {
    getMovesPGN(game : _Game, \n displayOptions? : PGNDisplayOptions) string
  }
  getMovesPGN --> getMovesPGNInfo

  class getMovesPGNInfo {
    getMovesPGNInfo(game : _Game) PGNToken[]
  }

  class getChecks {
    getChecks(game : _Game) Move[]
  }
  getChecks --> pass

  class getChecksPGN {
    getChecksPGN(game : _Game, \n displayOptions? : PGNDisplayOptions) string
  }
  getChecksPGN --> getChecksPGNInfo

  class getChecksPGNInfo {
    getChecksPGNInfo(game : _Game) PGNToken[]
  }

  class createGame {
    createGame(variant? : VariantFullString | \n VariantString | \n VariantShortString) _Game
  }

  class getMetadataFromPGN {
    getMetadataFromPGN(pgn : string) PGNTag[]
  }
```
