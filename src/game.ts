// import { _Game, PieceString, PlayerString, VariantString, VariantFullString, VariantShortString } from './@types/types';

// export function createGame(variant: (VariantFullString | VariantString | VariantShortString) = VariantString.Standard): _Game {
//   let baseGame: _Game = {
//     actionHistory: [],
//     moveBuffer: [],
//     availablePromotion: [
//       PieceString.Queen,
//       PieceString.Rook,
//       PieceString.Bishop,
//       PieceString.Knight
//     ],
//     board: {
//       width: 8,
//       height: 8,
//       timelines: [
//         {
//           timeline: 0,
//           steps: [
//             {
//               step: 0,
//               pieces: [
//                 {
//                   piece: PieceString.Rook,
//                   player: PlayerString.White,
//                   file: 0,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Knight,
//                   player: PlayerString.White,
//                   file: 1,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Bishop,
//                   player: PlayerString.White,
//                   file: 2,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Queen,
//                   player: PlayerString.White,
//                   file: 3,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.King,
//                   player: PlayerString.White,
//                   file: 4,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Bishop,
//                   player: PlayerString.White,
//                   file: 5,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Knight,
//                   player: PlayerString.White,
//                   file: 6,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Rook,
//                   player: PlayerString.White,
//                   file: 7,
//                   rank: 0,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 0,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 1,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 2,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 3,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 4,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 5,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 6,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.White,
//                   file: 7,
//                   rank: 1,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 0,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 1,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 2,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 3,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 4,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 5,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 6,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Pawn,
//                   player: PlayerString.Black,
//                   file: 7,
//                   rank: 6,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Rook,
//                   player: PlayerString.Black,
//                   file: 0,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Knight,
//                   player: PlayerString.Black,
//                   file: 1,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Bishop,
//                   player: PlayerString.Black,
//                   file: 2,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Queen,
//                   player: PlayerString.Black,
//                   file: 3,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.King,
//                   player: PlayerString.Black,
//                   file: 4,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Bishop,
//                   player: PlayerString.Black,
//                   file: 5,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Knight,
//                   player: PlayerString.Black,
//                   file: 6,
//                   rank: 7,
//                   moved: false
//                 },
//                 {
//                   piece: PieceString.Rook,
//                   player: PlayerString.Black,
//                   file: 7,
//                   rank: 7,
//                   moved: false
//                 }
//               ]
//             }
//           ]
//         }
//       ]
//     }
//   };
//   // Return baseGame unmodified if input does not match any variant that isn't standard
//   return baseGame;
// }