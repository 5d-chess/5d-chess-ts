export const PieceString = {
  Pawn: 'P',
  Brawn: 'W',
  Knight: 'N',
  Rook: 'R',
  Bishop: 'B',
  Unicorn: 'U',
  Dragon: 'D',
  Princess: 'S',
  CommonKing: 'C',
  Queen: 'Q',
  King: 'K',
  RoyalQueen: 'Y',
}

export type PieceString = 'P' | 'W' | 'N' | 'R' | 'B' | 'U' | 'D' | 'S' | 'C' | 'Q' | 'K' | 'Y'

export enum PlayerString {
  'white',
  'black'
}

export type Position = {
  timeline: number,
  step: number,
  file: number,
  rank: number
};

export type Piece = {
  rank: number,
  file: number,
  moved: boolean
  piece: PieceString,
  player: PlayerString,
  position?: Position,
};

export type Step = {
  step: number,
  pieces: Piece[]
  hash?: string,
  width?: number,
  height?: number,
};

export type Timeline = {
  timeline: number,
  steps: Step[]
  hash?: string,
  active?: boolean,
  present?: boolean,
};

export type Board = {
  width: number,
  height: number,
  timelines: Timeline[]
  hash?: string,
};

export type Move = {
  startPosition: Position,
  endPosition: Position,
  realEndPosition?: Position,
  capture?: Position,
  promotion?: PieceString,
  Castling?: {
    startPosition: Position,
    endPosition: Position,
    realEndPosition?: Position
  }
};

export type Action = Move[];


export type Game = {
  board: Board
  moveBuffer: Move[],
  actionHistory: Action[],
  availablePromotion: PieceString[],
  currentPlayer?: PlayerString,
};

export type Session = {
  game: Game
};

export type PGN = {
  metadata: {
    tags: PGNTag
    comments?: PGNComment
  }
  pgn: PGNString
}

export type PGNTag = {
  size: string
  variant: string
  fen: string
  promotionPieces: string[]
}

export type PGNComment = {
  comments: string[]
  activeTimeline: string
  newTimeline: string
  arrows: string[]
  highlights: string[]
}

export type PGNString = {
  pgnMoves: string
  currentPlayer: string
}