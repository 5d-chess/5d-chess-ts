// import { PieceString, _Position } from './types';

// export enum PGNFragmentType {
//   SuperPhysicalSource = 'superphysical_source',
//   Castling = 'castling',
//   SourcePiece = 'source_piece',
//   PhysicalSource = 'physical_source',
//   JumpSeparator = 'jump_separator',
//   CaptureIndicator = 'capture_indicator',
//   SuperPhysicalDestination = 'superphysical_destination',
//   PhysicalDestination = 'physical_destination',
//   PromotionIndicator = 'promotion_indicator',
//   CheckIndicator = 'check_indicator',
//   SoftmateIndicator = 'softmate_indicator',
//   CheckmateIndicator = 'checkmate_indicator',
//   BranchingPresentIndicator = 'branching_present_indicator',
//   ActiveTimelineIndicator = 'active_timeline_indicator',
//   NewTimelineIndicator = 'new_timeline_indicator',
//   ActionSeparator = 'action_separator',
//   CommentStart = 'comment_start',
//   CommentString = 'comment_string',
//   CommentHighlight = 'comment_highlight',
//   CommentArrow = 'comment_arrow',
//   CommentEnd = 'comment_end',
//   TagStart = 'tag_start',
//   TagKey = 'tag_key',
//   TagValue = 'tag_value',
//   TagEnd = 'tag_end',
//   FenStart = 'fen_start',
//   FenRow = 'fen_row',
//   FenRowDelimiter = 'fen_row_delimiter',
//   FenTimeline = 'fen_timeline',
//   FenTurn = 'fen_turn',
//   FenTurnPlayer = 'fen_turn_player',
//   FenFieldDelimiter = 'fen_field_delimiter',
//   FenEnd = 'fen_end'
// }

// export type PGNFragment = {
//   type: PGNFragmentType,
//   sourcePosition?: _Position,
//   sourceSuperPhysicalRequired?: boolean,
//   isRegularCastling?: boolean,
//   isRegularLongCastling?: boolean,
//   sourcePieceType?: PieceString,
//   sourceFileRequired?: boolean,
//   sourceRankRequired?: boolean,
//   isBranching?: boolean,
//   destinationPosition?: _Position,
//   promotionPieceType?: PieceString,
//   commentString?: string,
//   commentHighlightType?: PGNCommentHighlightType,
//   commentArrowMiddlePosition?: _Position,
//   commentArrowType?: PGNCommentArrowType
// };

// export enum PGNTokenType {
//   Move = 'move',
//   ActiveTimelineIndicator = 'active_timeline_indicator',
//   NewTimelineIndicator = 'new_timeline_indicator',
//   ActionSeparator = 'action_separator',
//   Comment = 'comment',
//   Tag = 'tag',
//   Fen = 'fen'
// }

// export type PGNToken = {
//   type: PGNTokenType,
//   /** Used by move and comment to build string */
//   fragments?: PGNFragment[],
//   /** Used by timeline activation tokens to display the correct number */
//   step?: number,
//   /** Used by timeline creation tokens to display the correct number */
//   timeline?: number,
//   /** Used by action separators to display the correct number */
//   actionIndex?: number,
//   /** Used by action separators to check if black first indicator is needed */
//   blackFirst?: boolean
// };

// export type PGNDisplayOptions = {
//   /** Show player letter in action separator */
//   playerLetterAction: boolean,
//   /** Use slash as action separator */
//   simpleAction: boolean,
//   /** Show timeline activation tokens */
//   activeTimeline: boolean,
//   /** Show timeline creation tokens */
//   newTimeline: boolean,
//   /** Show superphysical coordinates with (L<l> T<t>) format, use (<l>T<t>) format if false */
//   extraLTimeline: boolean,
//   /** Show source position file coordinate even if not required */
//   sourceFile: boolean,
//   /** Show source position rank coordinate even if not required */
//   sourceRank: boolean,
//   /** Show source superphysical coordinate even if not required */
//   sourceSuperPhysical: boolean,
//   /** Show check indicator */
//   check: boolean,
//   /** Show softmate indicator */
//   softmate: boolean,
//   /** Show checkmate indicator */
//   checkmate: boolean,
//   /** Show branching present indicator */
//   branchingPresent: boolean,
//   /** Change PGN output to minimal mode */
//   minimalMode: boolean,
//   /** Show piece type indicator while in minimal mode */
//   minimalPiece: boolean
// };

// export type PGNTag = {
//   name: string,
//   value: string
// };

// export type FEN = {
//   timeline: number,
//   step: number,
//   value: string
// };

// export enum PGNCommentHighlightType {
//   Source = 'source',
//   Move = 'move',
//   Capture = 'capture',
//   Color1 = 'color1',
//   Color2 = 'color2',
//   Color3 = 'color3',
//   Color4 = 'color4'
// }

// export enum PGNCommentArrowType {
//   Move = 'move',
//   Check = 'check',
//   Color1 = 'color1',
//   Color2 = 'color2',
//   Color3 = 'color3',
//   Color4 = 'color4'
// }

/*
Fragment Order

Combined:
 MSN - SuperPhysicalSource
  S  - Castling
 MSN - SourcePiece
 MSN - PhysicalSource
   N - JumpSeparator
  SN - CaptureIndicator
 M N - SuperPhysicalDestination
 MSN - PhysicalDestination
 MSN - PromotionIndicator
  SN - CheckIndicator
  SN - SoftmateIndicator
  SN - CheckmateIndicator
   N - BranchingPresentIndicator

Minimal:
 - SuperPhysicalSource
 - SourcePiece
 - PhysicalSource
 - SuperPhysicalDestination
 - PhysicalDestination
 - PromotionIndicator

Spatial Move:
 - SuperPhysicalSource
 - Castling
 - SourcePiece
 - PhysicalSource
 - CaptureIndicator
 - PhysicalDestination
 - PromotionIndicator
 - CheckIndicator
 - SoftmateIndicator
 - CheckmateIndicator

Non-Spatial Move:
 - SuperPhysicalSource
 - SourcePiece
 - PhysicalSource
 - JumpSeparator
 - CaptureIndicator
 - SuperPhysicalDestination
 - PhysicalDestination
 - PromotionIndicator
 - CheckIndicator
 - SoftmateIndicator
 - CheckmateIndicator
 - BranchingPresentIndicator
*/