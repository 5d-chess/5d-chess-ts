// import { PGNDisplayOptions, PGNTokenType } from '../@types/pgn';
// import { getStringFromPGNToken } from '../pgn';

// test('PGN Token to String Conversion Tests - ActionSeparator - Regular Mode', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('1.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0
//   }, displayOptions)).toBe('1.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('/');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: true
//   }, displayOptions)).toBe('1b.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 2,
//     blackFirst: true
//   }, displayOptions)).toBe('2w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 3,
//     blackFirst: true
//   }, displayOptions)).toBe('/');
//   expect(() => {
//     expect(getStringFromPGNToken({
//       type: PGNTokenType.ActionSeparator
//     }, displayOptions));
//   }).toThrowError(ReferenceError);
//   displayOptions.simpleAction = false;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('1w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('1b.');
//   displayOptions.simpleAction = true;
//   displayOptions.playerLetterAction = true;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('1w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('/');
// });

// test('PGN Token to String Conversion Tests - ActionSeparator - Minimal Mode', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0
//   }, displayOptions)).toBe('w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('b.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: true
//   }, displayOptions)).toBe('b.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 2,
//     blackFirst: true
//   }, displayOptions)).toBe('w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 3,
//     blackFirst: true
//   }, displayOptions)).toBe('b.');
//   expect(() => {
//     expect(getStringFromPGNToken({
//       type: PGNTokenType.ActionSeparator
//     }, displayOptions));
//   }).toThrowError(ReferenceError);
//   displayOptions.simpleAction = false;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('b.');
//   displayOptions.simpleAction = true;
//   displayOptions.playerLetterAction = true;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 0,
//     blackFirst: false
//   }, displayOptions)).toBe('w.');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActionSeparator,
//     actionIndex: 1,
//     blackFirst: false
//   }, displayOptions)).toBe('b.');
// });

// test('PGN Token to String Conversion Tests - ActiveTimelineIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActiveTimelineIndicator,
//     step: 0
//   }, displayOptions)).toBe('(~T1)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActiveTimelineIndicator,
//     step: 1
//   }, displayOptions)).toBe('(~T1)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActiveTimelineIndicator,
//     step: 2
//   }, displayOptions)).toBe('(~T2)');
//   expect(() => {
//     expect(getStringFromPGNToken({
//       type: PGNTokenType.ActiveTimelineIndicator
//     }, displayOptions));
//   }).toThrowError(ReferenceError);
//   displayOptions.minimalMode = true;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.ActiveTimelineIndicator,
//     step: 0
//   }, displayOptions)).toBe('');
// });

// test('PGN Token to String Conversion Tests - NewTimelineIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: 0
//   }, displayOptions)).toBe('(>L0)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: 1
//   }, displayOptions)).toBe('(>L+0)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: -1
//   }, displayOptions)).toBe('(>L-0)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: 2
//   }, displayOptions)).toBe('(>L1)');
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: -2
//   }, displayOptions)).toBe('(>L-1)');
//   expect(() => {
//     expect(getStringFromPGNToken({
//       type: PGNTokenType.NewTimelineIndicator
//     }, displayOptions));
//   }).toThrowError(ReferenceError);
//   displayOptions.minimalMode = true;
//   expect(getStringFromPGNToken({
//     type: PGNTokenType.NewTimelineIndicator,
//     timeline: 0
//   }, displayOptions)).toBe('');
// });