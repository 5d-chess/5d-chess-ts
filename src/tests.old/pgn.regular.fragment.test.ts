// import { PGNDisplayOptions, PGNFragment, PGNFragmentType } from '../@types/pgn';
// import { PieceString } from '../@types/types';
// import { getStringFromPGNFragment } from '../pgn';

// test('PGN Fragment to String Conversion Tests - Regular Mode - No Type', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(() => {
//     getStringFromPGNFragment({} as PGNFragment, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - SuperPhysicalSource', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     sourceSuperPhysicalRequired: true
//   }, displayOptions)).toBe('(0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 1, step: 1, file: 0, rank: 0 },
//     sourceSuperPhysicalRequired: true
//   }, displayOptions)).toBe('(+0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: -1, step: 2, file: 0, rank: 0 },
//     sourceSuperPhysicalRequired: true
//   }, displayOptions)).toBe('(-0T2)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 2, step: 10, file: 0, rank: 0 },
//     sourceSuperPhysicalRequired: true
//   }, displayOptions)).toBe('(1T6)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SuperPhysicalSource
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.sourceSuperPhysical = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(0T1)');
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(L1 T6)');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - Castling', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.Castling,
//     isRegularCastling: true,
//     isRegularLongCastling: false
//   }, displayOptions)).toBe('O-O');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.Castling,
//     isRegularCastling: true,
//     isRegularLongCastling: true
//   }, displayOptions)).toBe('O-O-O');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.Castling
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - SourcePiece', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Pawn
//   }, displayOptions)).toBe('P');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Princess
//   }, displayOptions)).toBe('S');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Dragon
//   }, displayOptions)).toBe('D');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SourcePiece
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - PhysicalSource', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     sourceFileRequired: true,
//     sourceRankRequired: true
//   }, displayOptions)).toBe('a1');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 3, rank: 4 },
//     sourceFileRequired: true,
//     sourceRankRequired: true
//   }, displayOptions)).toBe('d5');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 22, rank: 99 },
//     sourceFileRequired: true,
//     sourceRankRequired: true
//   }, displayOptions)).toBe('w100');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     sourceFileRequired: true,
//     sourceRankRequired: false
//   }, displayOptions)).toBe('a');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     sourceFileRequired: false,
//     sourceRankRequired: true
//   }, displayOptions)).toBe('1');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     sourceFileRequired: false,
//     sourceRankRequired: false
//   }, displayOptions)).toBe('');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: 26, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: -1, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: 0, rank: -1 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.sourceFile = true;
//   displayOptions.sourceRank = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('a1');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - JumpSeparator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.JumpSeparator
//   }, displayOptions)).toBe('>');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.JumpSeparator,
//     isBranching: false
//   }, displayOptions)).toBe('>');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.JumpSeparator,
//     isBranching: true
//   }, displayOptions)).toBe('>>');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - CaptureIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CaptureIndicator
//   }, displayOptions)).toBe('x');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - SuperPhysicalDestination', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 1, step: 1, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(+0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: -1, step: 2, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(-0T2)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(1T6)');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SuperPhysicalDestination
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(L1 T6)');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - PhysicalDestination', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('a1');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 3, rank: 4 }
//   }, displayOptions)).toBe('d5');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 22, rank: 99 }
//   }, displayOptions)).toBe('w100');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: 26, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: -1, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: 0, rank: -1 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - PromotionIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Rook
//   }, displayOptions)).toBe('=R');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Princess
//   }, displayOptions)).toBe('=S');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Dragon
//   }, displayOptions)).toBe('=D');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PromotionIndicator
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - CheckIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CheckIndicator
//   }, displayOptions)).toBe('+');
//   displayOptions.check = false;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CheckIndicator
//   }, displayOptions)).toBe('');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - SoftmateIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SoftmateIndicator
//   }, displayOptions)).toBe('*');
//   displayOptions.softmate = false;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SoftmateIndicator
//   }, displayOptions)).toBe('');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - CheckmateIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CheckmateIndicator
//   }, displayOptions)).toBe('#');
//   displayOptions.checkmate = false;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CheckmateIndicator
//   }, displayOptions)).toBe('');
// });

// test('PGN Fragment to String Conversion Tests - Regular Mode - BranchingPresentIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.BranchingPresentIndicator
//   }, displayOptions)).toBe('~');
//   displayOptions.branchingPresent = false;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.BranchingPresentIndicator
//   }, displayOptions)).toBe('');
// });