// import { PGNDisplayOptions, PGNFragmentType } from '../@types/pgn';
// import { PieceString } from '../@types/types';
// import { getStringFromPGNFragment } from '../pgn';

// test('PGN Fragment to String Conversion Tests - Minimal Mode - SuperPhysicalSource', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 1, step: 1, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(+0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: -1, step: 2, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(-0T2)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(1T6)');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SuperPhysicalSource
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalSource,
//     sourcePosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(1T6)');
// });

// test('PGN Fragment to String Conversion Tests - Minimal Mode - SourcePiece', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Pawn
//   }, displayOptions)).toBe('P');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Princess
//   }, displayOptions)).toBe('S');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Dragon
//   }, displayOptions)).toBe('D');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SourcePiece
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.minimalPiece = false;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SourcePiece,
//     sourcePieceType: PieceString.Dragon
//   }, displayOptions)).toBe('');
// });

// test('PGN Fragment to String Conversion Tests - Minimal Mode - PhysicalSource', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('a1');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 3, rank: 4 }
//   }, displayOptions)).toBe('d5');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalSource,
//     sourcePosition: { timeline: 0, step: 0, file: 22, rank: 99 }
//   }, displayOptions)).toBe('w100');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: 26, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: -1, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource,
//       sourcePosition: { timeline: 0, step: 0, file: 0, rank: -1 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalSource
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Minimal Mode - SuperPhysicalDestination', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 1, step: 1, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(+0T1)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: -1, step: 2, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(-0T2)');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(1T6)');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.SuperPhysicalDestination
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.SuperPhysicalDestination,
//     destinationPosition: { timeline: 2, step: 10, file: 0, rank: 0 }
//   }, displayOptions)).toBe('(1T6)');
// });

// test('PGN Fragment to String Conversion Tests - Minimal Mode - PhysicalDestination', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//   }, displayOptions)).toBe('a1');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 3, rank: 4 }
//   }, displayOptions)).toBe('d5');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PhysicalDestination,
//     destinationPosition: { timeline: 0, step: 0, file: 22, rank: 99 }
//   }, displayOptions)).toBe('w100');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: 26, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: -1, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination,
//       destinationPosition: { timeline: 0, step: 0, file: 0, rank: -1 }
//     }, displayOptions);
//   }).toThrowError(RangeError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PhysicalDestination
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Minimal Mode - PromotionIndicator', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: true,
//     minimalPiece: true
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Rook
//   }, displayOptions)).toBe('=R');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Princess
//   }, displayOptions)).toBe('=S');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.PromotionIndicator,
//     promotionPieceType: PieceString.Dragon
//   }, displayOptions)).toBe('=D');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.PromotionIndicator
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
// });