// import { variantToBoardTag } from '../pgn';

// test('PGN Metadata Tag Tests - Board tags from Standard variant strings', () => {
//   expect(variantToBoardTag('Standard').value).toBe('Standard');
//   expect(variantToBoardTag('Standard').value).toBe('Standard');
//   expect(variantToBoardTag('standard').value).toBe('Standard');
// });

// test('PGN Metadata Tag Tests - Board tags from Defended Pawn variant strings', () => {
//   expect(variantToBoardTag('Standard - Defended Pawn').value).toBe('Standard - Defended Pawn');
//   expect(variantToBoardTag('Defended Pawn').value).toBe('Standard - Defended Pawn');
//   expect(variantToBoardTag('defended_pawn').value).toBe('Standard - Defended Pawn');
// });

// test('PGN Metadata Tag Tests - Board tags from Half Reflected variant strings', () => {
//   expect(variantToBoardTag('Standard - Half Reflected').value).toBe('Standard - Half Reflected');
//   expect(variantToBoardTag('Half Reflected').value).toBe('Standard - Half Reflected');
//   expect(variantToBoardTag('half_reflected').value).toBe('Standard - Half Reflected');
// });

// test('PGN Metadata Tag Tests - Board tags from Princess variant strings', () => {
//   expect(variantToBoardTag('Standard - Princess').value).toBe('Standard - Princess');
//   expect(variantToBoardTag('Princess').value).toBe('Standard - Princess');
//   expect(variantToBoardTag('princess').value).toBe('Standard - Princess');
// });

// test('PGN Metadata Tag Tests - Board tags from Turn Zero variant strings', () => {
//   expect(variantToBoardTag('Standard - Turn Zero').value).toBe('Standard - Turn Zero');
//   expect(variantToBoardTag('Turn Zero').value).toBe('Standard - Turn Zero');
//   expect(variantToBoardTag('turn_zero').value).toBe('Standard - Turn Zero');
// });

// test('PGN Metadata Tag Tests - Board tags from Two Timelines variant strings', () => {
//   expect(variantToBoardTag('Standard - Two Timelines').value).toBe('Standard - Two Timelines');
//   expect(variantToBoardTag('Two Timelines').value).toBe('Standard - Two Timelines');
//   expect(variantToBoardTag('two_timelines').value).toBe('Standard - Two Timelines');
// });

// test('PGN Metadata Tag Tests - Board tags from Reversed Royalty variant strings', () => {
//   expect(variantToBoardTag('Standard - Reversed Royalty').value).toBe('Standard - Reversed Royalty');
//   expect(variantToBoardTag('Reversed Royalty').value).toBe('Standard - Reversed Royalty');
//   expect(variantToBoardTag('reversed_royalty').value).toBe('Standard - Reversed Royalty');
// });

// test('PGN Metadata Tag Tests - Board tags from Custom variant strings', () => {
//   expect(variantToBoardTag('Custom').value).toBe('custom');
//   expect(variantToBoardTag('Custom').value).toBe('custom');
//   expect(variantToBoardTag('custom').value).toBe('custom');
// });

// test('PGN Metadata Tag Tests - Board tags from Random user input', () => {
//   expect(variantToBoardTag('random').value).toBe('custom');
// });