// import { PGNCommentArrowType, PGNCommentHighlightType, PGNDisplayOptions, PGNFragmentType } from '../@types/pgn';
// import { getStringFromPGNFragment } from '../pgn';

// test('PGN Fragment to String Conversion Tests - Comment Fragments - CommentStart', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentStart
//   }, displayOptions)).toBe('{');
// });

// test('PGN Fragment to String Conversion Tests - Comment Fragments - CommentString', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentString,
//     commentString: 'Test Comment'
//   }, displayOptions)).toBe('Test Comment');
//   expect(() => {
//     expect(getStringFromPGNFragment({
//       type: PGNFragmentType.CommentString
//     }, displayOptions));
//   }).toThrowError(ReferenceError);
// });

// test('PGN Fragment to String Conversion Tests - Comment Fragments - CommentHighlight', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentHighlight,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     commentHighlightType: PGNCommentHighlightType.Source
//   }, displayOptions)).toBe('[(0T1)a1 source]');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentHighlight,
//     sourcePosition: { timeline: 1, step: 1, file: 3, rank: 4 },
//     commentHighlightType: PGNCommentHighlightType.Move
//   }, displayOptions)).toBe('[(+0T1)d5 move]');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.CommentHighlight
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.CommentHighlight,
//       sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentHighlight,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     commentHighlightType: PGNCommentHighlightType.Source
//   }, displayOptions)).toBe('[(L0 T1)a1 source]');
// });

// test('PGN Fragment to String Conversion Tests - Comment Fragments - CommentArrow', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentArrow,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     destinationPosition: { timeline: 0, step: 0, file: 3, rank: 4 },
//     commentArrowType: PGNCommentArrowType.Move
//   }, displayOptions)).toBe('[(0T1)a1 (0T1)d5 move]');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentArrow,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     commentArrowMiddlePosition: { timeline: 0, step: 0, file: 3, rank: 4 },
//     destinationPosition: { timeline: 2, step: 0, file: 3, rank: 4 },
//     commentArrowType: PGNCommentArrowType.Check
//   }, displayOptions)).toBe('[(0T1)a1 (0T1)d5 (1T1)d5 check]');
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.CommentArrow
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.CommentArrow,
//       sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 }
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   expect(() => {
//     getStringFromPGNFragment({
//       type: PGNFragmentType.CommentArrow,
//       sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//       destinationPosition: { timeline: 0, step: 1, file: 3, rank: 4 }
//     }, displayOptions);
//   }).toThrowError(ReferenceError);
//   displayOptions.extraLTimeline = true;
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentArrow,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     destinationPosition: { timeline: 0, step: 0, file: 3, rank: 4 },
//     commentArrowType: PGNCommentArrowType.Move
//   }, displayOptions)).toBe('[(L0 T1)a1 (L0 T1)d5 move]');
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentArrow,
//     sourcePosition: { timeline: 0, step: 0, file: 0, rank: 0 },
//     commentArrowMiddlePosition: { timeline: 0, step: 0, file: 3, rank: 4 },
//     destinationPosition: { timeline: 2, step: 0, file: 3, rank: 4 },
//     commentArrowType: PGNCommentArrowType.Check
//   }, displayOptions)).toBe('[(L0 T1)a1 (L0 T1)d5 (L1 T1)d5 check]');
// });

// test('PGN Fragment to String Conversion Tests - Comment Fragments - CommentEnd', () => {
//   let displayOptions: PGNDisplayOptions = {
//     playerLetterAction: false,
//     simpleAction: true,
//     activeTimeline: true,
//     newTimeline: true,
//     extraLTimeline: false,
//     sourceFile: false,
//     sourceRank: false,
//     sourceSuperPhysical: false,
//     check: true,
//     softmate: true,
//     checkmate: true,
//     branchingPresent: true,
//     minimalMode: false,
//     minimalPiece: false
//   };
//   expect(getStringFromPGNFragment({
//     type: PGNFragmentType.CommentEnd
//   }, displayOptions)).toBe('}');
// });