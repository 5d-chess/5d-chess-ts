// import { PGNDisplayOptions, PGNFragment, PGNFragmentType, PGNTag, PGNToken, PGNTokenType } from './@types/pgn';
// import { PlayerString, VariantFullString, VariantShortString, VariantString } from './@types/types';

// export function variantToBoardTag(variant: string | VariantFullString | VariantString | VariantShortString): PGNTag {

//   if (!isNaN(+variant)) return {
//     name: 'Board',
//     value: 'custom'
//   };

//   const index = VariantFullString[variant] ?? VariantShortString[variant] ?? VariantString[variant];

//   if (index === undefined || VariantFullString[index] === VariantFullString[VariantFullString.Custom]) return {
//     name: 'Board',
//     value: 'custom'
//   };

//   return {
//     name: 'Board',
//     value: VariantFullString[index],
//   };
// }

function getTimelineStringFromTimeline(timeline: number): string {
  if (timeline % 2 === 0) return Math.round(timeline / 2).toFixed(0);
  if (timeline === 1) return '+0';
  if (timeline === -1) return '-0';
  if (timeline < 0) return Math.ceil(timeline / 2).toFixed(0);
  return Math.floor(timeline / 2).toFixed(0);
}

export function getTurnStringFromStep(step: number): string {
  return Math.round((step + 1) / 2).toFixed(0);
}

// export function getTurnPlayerFromStep(step: number): PlayerString {
//   return step % 2 === 0 ? PlayerString.White : PlayerString.Black;
// }

export function getSANFromFileRank(file: number, rank: number) {
  if (file > 22) {
    throw RangeError('File number above 22 is not permitted');
  }
  if (file < 0) {
    throw RangeError('File number below 0 is not permitted');
  }
  if (rank < 0) {
    throw RangeError('Rank number below 0 is not permitted');
  }
  return `${String.fromCharCode(97 + file)}${(rank + 1).toFixed(0)}`;
}

function getActionStringFromActionIndex(actionIndex: number): string {
  return Math.round((actionIndex + 1) / 2).toFixed(0);
}

// function getActionPlayerFromStep(actionIndex: number): PlayerString {
//   return actionIndex % 2 === 0 ? PlayerString.White : PlayerString.Black;
// }

// function validatePGNFragment(fragment: PGNFragment): void {
//   if (typeof fragment.type === 'undefined') {
//     throw ReferenceError('type is required on PGNFragments');
//   }
//   if (fragment.type === PGNFragmentType.SuperPhysicalSource) {
//     if (typeof fragment.sourcePosition === 'undefined') {
//       throw ReferenceError('sourcePosition is required on SuperPhysicalSource PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.Castling) {
//     if (typeof fragment.isRegularCastling === 'undefined') {
//       throw ReferenceError('isRegularCastling is required on Castling PGNFragments');
//     }
//     if (typeof fragment.isRegularLongCastling === 'undefined') {
//       throw ReferenceError('isRegularLongCastling is required on Castling PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.SourcePiece) {
//     if (typeof fragment.sourcePieceType === 'undefined') {
//       throw ReferenceError('sourcePieceType is required on SourcePiece PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.PhysicalSource) {
//     if (typeof fragment.sourcePosition === 'undefined') {
//       throw ReferenceError('sourcePosition is required on PhysicalSource PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.SuperPhysicalDestination) {
//     if (typeof fragment.destinationPosition === 'undefined') {
//       throw ReferenceError('destinationPosition is required on SuperPhysicalDestination PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.PhysicalDestination) {
//     if (typeof fragment.destinationPosition === 'undefined') {
//       throw ReferenceError('destinationPosition is required on PhysicalDestination PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.PromotionIndicator) {
//     if (typeof fragment.promotionPieceType === 'undefined') {
//       throw ReferenceError('promotionPieceType is required on PromotionIndicator PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.CommentString) {
//     if (typeof fragment.commentString === 'undefined') {
//       throw ReferenceError('commentString is required on CommentString PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.CommentHighlight) {
//     if (typeof fragment.sourcePosition === 'undefined') {
//       throw ReferenceError('sourcePosition is required on CommentHighlight PGNFragments');
//     }
//     if (typeof fragment.commentHighlightType === 'undefined') {
//       throw ReferenceError('commentHighlightType is required on CommentHighlight PGNFragments');
//     }
//   }
//   if (fragment.type === PGNFragmentType.CommentArrow) {
//     if (typeof fragment.sourcePosition === 'undefined') {
//       throw ReferenceError('sourcePosition is required on CommentArrow PGNFragments');
//     }
//     if (typeof fragment.destinationPosition === 'undefined') {
//       throw ReferenceError('destinationPosition is required on CommentArrow PGNFragments');
//     }
//     if (typeof fragment.commentArrowType === 'undefined') {
//       throw ReferenceError('commentArrowType is required on CommentArrow PGNFragments');
//     }
//   }
// }

// export function getStringFromPGNFragment(fragment: PGNFragment, displayOptions: PGNDisplayOptions): string {
//   validatePGNFragment(fragment);
//   if (fragment.type === PGNFragmentType.SuperPhysicalSource) {
//     if (displayOptions.sourceSuperPhysical || displayOptions.minimalMode || fragment.sourceSuperPhysicalRequired) {
//       let result = '(';
//       result += displayOptions.extraLTimeline && !displayOptions.minimalMode ? 'L' : '';
//       result += getTimelineStringFromTimeline(fragment.sourcePosition.timeline);
//       result += displayOptions.extraLTimeline && !displayOptions.minimalMode ? ' ' : '';
//       result += `T${getTurnStringFromStep(fragment.sourcePosition.step)})`;
//       return result;
//     }
//   }
//   if (fragment.type === PGNFragmentType.Castling) {
//     if (fragment.isRegularCastling) {
//       return 'O-O' + (fragment.isRegularLongCastling ? '-O' : '');
//     }
//   }
//   if (fragment.type === PGNFragmentType.SourcePiece) {
//     if (!displayOptions.minimalMode || (displayOptions.minimalMode && displayOptions.minimalPiece)) {
//       return fragment.sourcePieceType;
//     }
//   }
//   if (fragment.type === PGNFragmentType.PhysicalSource) {
//     if (!fragment.isRegularCastling) {
//       let result = '';
//       let san = getSANFromFileRank(fragment.sourcePosition.file, fragment.sourcePosition.rank);
//       if (displayOptions.sourceFile || displayOptions.minimalMode || fragment.sourceFileRequired) {
//         result += san.substring(0, 1);
//       }
//       if (displayOptions.sourceRank || displayOptions.minimalMode || fragment.sourceRankRequired) {
//         result += san.substring(1);
//       }
//       return result;
//     }
//   }
//   if (fragment.type === PGNFragmentType.JumpSeparator) {
//     return '>' + (fragment.isBranching ? '>' : '');
//   }
//   if (fragment.type === PGNFragmentType.CaptureIndicator) {
//     return 'x';
//   }
//   if (fragment.type === PGNFragmentType.SuperPhysicalDestination) {
//     let result = '(';
//     result += displayOptions.extraLTimeline && !displayOptions.minimalMode ? 'L' : '';
//     result += getTimelineStringFromTimeline(fragment.destinationPosition.timeline);
//     result += displayOptions.extraLTimeline && !displayOptions.minimalMode ? ' ' : '';
//     result += `T${getTurnStringFromStep(fragment.destinationPosition.step)})`;
//     return result;
//   }
//   if (fragment.type === PGNFragmentType.PhysicalDestination) {
//     return getSANFromFileRank(fragment.destinationPosition.file, fragment.destinationPosition.rank);
//   }
//   if (fragment.type === PGNFragmentType.PromotionIndicator) {
//     return '=' + fragment.promotionPieceType;
//   }
//   if (fragment.type === PGNFragmentType.CheckIndicator) {
//     if (displayOptions.check) {
//       return '+';
//     }
//   }
//   if (fragment.type === PGNFragmentType.SoftmateIndicator) {
//     if (displayOptions.softmate) {
//       return '*';
//     }
//   }
//   if (fragment.type === PGNFragmentType.CheckmateIndicator) {
//     if (displayOptions.checkmate) {
//       return '#';
//     }
//   }
//   if (fragment.type === PGNFragmentType.BranchingPresentIndicator) {
//     if (displayOptions.branchingPresent) {
//       return '~';
//     }
//   }
//   if (fragment.type === PGNFragmentType.CommentStart) {
//     return '{';
//   }
//   if (fragment.type === PGNFragmentType.CommentString) {
//     return fragment.commentString;
//   }
//   if (fragment.type === PGNFragmentType.CommentHighlight) {
//     let result = '[(';
//     result += displayOptions.extraLTimeline ? 'L' : '';
//     result += getTimelineStringFromTimeline(fragment.sourcePosition.timeline);
//     result += displayOptions.extraLTimeline ? ' ' : '';
//     result += `T${getTurnStringFromStep(fragment.sourcePosition.step)})`;
//     result += getSANFromFileRank(fragment.sourcePosition.file, fragment.sourcePosition.rank);
//     result += ' ';
//     result += `${fragment.commentHighlightType}]`;
//     return result;
//   }
//   if (fragment.type === PGNFragmentType.CommentArrow) {
//     let result = '[(';
//     result += displayOptions.extraLTimeline ? 'L' : '';
//     result += getTimelineStringFromTimeline(fragment.sourcePosition.timeline);
//     result += displayOptions.extraLTimeline ? ' ' : '';
//     result += `T${getTurnStringFromStep(fragment.sourcePosition.step)})`;
//     result += getSANFromFileRank(fragment.sourcePosition.file, fragment.sourcePosition.rank);
//     result += ' ';
//     if (typeof fragment.commentArrowMiddlePosition !== 'undefined') {
//       result += '(';
//       result += displayOptions.extraLTimeline ? 'L' : '';
//       result += getTimelineStringFromTimeline(fragment.commentArrowMiddlePosition.timeline);
//       result += displayOptions.extraLTimeline ? ' ' : '';
//       result += `T${getTurnStringFromStep(fragment.commentArrowMiddlePosition.step)})`;
//       result += getSANFromFileRank(fragment.commentArrowMiddlePosition.file, fragment.commentArrowMiddlePosition.rank);
//       result += ' ';
//     }
//     result += '(';
//     result += displayOptions.extraLTimeline ? 'L' : '';
//     result += getTimelineStringFromTimeline(fragment.destinationPosition.timeline);
//     result += displayOptions.extraLTimeline ? ' ' : '';
//     result += `T${getTurnStringFromStep(fragment.destinationPosition.step)})`;
//     result += getSANFromFileRank(fragment.destinationPosition.file, fragment.destinationPosition.rank);
//     result += ` ${fragment.commentArrowType}]`;
//     return result;
//   }
//   if (fragment.type === PGNFragmentType.CommentEnd) {
//     return '}';
//   }
//   return '';
// }

// function validatePGNToken(token: PGNToken) {
//   if (typeof token.type === 'undefined') {
//     throw ReferenceError('type is required on PGNTokens');
//   }
//   if (token.type === PGNTokenType.Move) {
//     if (typeof token.fragments === 'undefined') {
//       throw ReferenceError('fragments is required on Move PGNTokens');
//     }
//   }
//   if (token.type === PGNTokenType.ActionSeparator) {
//     if (typeof token.actionIndex === 'undefined') {
//       throw ReferenceError('actionIndex is required on ActionSeparator PGNTokens');
//     }
//   }
//   if (token.type === PGNTokenType.ActiveTimelineIndicator) {
//     if (typeof token.step === 'undefined') {
//       throw ReferenceError('step is required on ActiveTimelineIndicator PGNTokens');
//     }
//   }
//   if (token.type === PGNTokenType.NewTimelineIndicator) {
//     if (typeof token.timeline === 'undefined') {
//       throw ReferenceError('timeline is required on NewTimelineIndicator PGNTokens');
//     }
//   }
//   if (token.type === PGNTokenType.Comment) {
//     if (typeof token.fragments === 'undefined') {
//       throw ReferenceError('fragments is required on Comment PGNTokens');
//     }
//   }
// }

// export function getStringFromPGNToken(token: PGNToken, displayOptions: PGNDisplayOptions): string {
//   validatePGNToken(token);
//   if (token.type === PGNTokenType.ActionSeparator) {
//     if (!displayOptions.minimalMode && displayOptions.simpleAction) {
//       if (
//         getActionPlayerFromStep(token.actionIndex) === PlayerString.Black &&
//         ((!token.blackFirst && token.actionIndex >= 1) || (token.actionIndex > 1))
//       ) {
//         return '/';
//       }
//     }
//     let result = '';
//     if (!displayOptions.minimalMode) {
//       result += getActionStringFromActionIndex(token.actionIndex);
//     }
//     if (
//       displayOptions.minimalMode ||
//       (token.actionIndex === 1 && token.blackFirst) ||
//       (token.actionIndex === 2 && token.blackFirst) ||
//       displayOptions.playerLetterAction ||
//       !displayOptions.simpleAction
//     ) {
//       result += getActionPlayerFromStep(token.actionIndex).substring(0, 1);
//     }
//     result += '.';
//     return result;
//   }
//   if (token.type === PGNTokenType.ActiveTimelineIndicator) {
//     if (!displayOptions.minimalMode) {
//       return `(~T${getTurnStringFromStep(token.step)})`;
//     }
//   }
//   if (token.type === PGNTokenType.NewTimelineIndicator) {
//     if (!displayOptions.minimalMode) {
//       return `(>L${getTimelineStringFromTimeline(token.timeline)})`;
//     }
//   }
//   if (token.type === PGNTokenType.Move || token.type === PGNTokenType.Comment) {
//     let result = '';
//     for (let fragment of token.fragments) {
//       result += getStringFromPGNFragment(fragment, displayOptions);
//     }
//     return result;
//   }
//   return '';
// }
