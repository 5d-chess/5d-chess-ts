Types:

- Chess
  - Data:
    - Session
    - Game
    - Board
    - Piece
    - Move
    - Action
    - 5DPGN
  - Methods:
    - 

- 5DPGN (simplifying a 5d chess game and metadata into a PGN String (string))
  - Metadata
    - Tags
      - Size
      - Board Variant
      - FEN (starting FEN)
      - Promotion Pieces (Pieces that a Pawn or Brawn can promote to)
    - Comments
      - Custom Comments
      - Active Timeline Indicator
      - New Timeline Indicator
      - (CI5D) Arrows
      - (CI5D) Highlights
  - Game
    - PGN
      - Action Indicator(Current Player)
      - PGN Moves